
# Java Application Container Image with Hot-Reloading

This project creates a Docker image that has uses both the [Jetbrains JDK](https://github.com/JetBrains/JetBrainsRuntime) and [Hotswap Agent](http://hotswapagent.org/) and the Java JDWP debugger.  The combination of these utilities allows for the hot-reloading of Java resources and classes to enable rapid application development and debugging capabilties.  As a Docker image, containers can be started to facilitate development without requiring complicated installations on developer workstations.

It is expected that containers are configured in Maven using the Fabric8 or Spotify Docker plugins.  These configurations should expose the 8000 (debugger) port and any other port that the application may open.  Most importantly, it should mount/bind the source code to the following possible paths.

| Directory                         | Type      | Hot-Reloaded |
| --------------------------------- |:---------:|:------------:|
| `/var/lib/jvm/dev/classes`        | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra1` | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra2` | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra3` | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra4` | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra5` | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra6` | Classpath | Yes          |
| `/var/lib/jvm/dev/classes-extra7` | Classpath | Yes          |
| `/var/lib/jvm/dev/lib`            | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra1`     | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra2`     | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra3`     | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra4`     | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra5`     | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra6`     | JARs      | No           |
| `/var/lib/jvm/dev/lib-extra7`     | JARs      | No           |

You may include your own `hotswap-agent.properties` in any of the `classes` folders.  The one loaded by default is usually sufficient.

You can specify the following environment variables when running the container.

| Environment Variable      | Default Value | Description |
| ------------------------- | ------------- | ----------- |
| `JAVA_MEMORY_INIT`        | `128m`        | The initial and minimum JVM memory. |
| `JAVA_MEMORY_MAX`         | `512m`        | The maximum JVM memory allowed. |
| `ENABLE_HOTSWAP`          | `true`        | Set to `false` to disable the HotSwap agent. |
| `DISABLE_HOTSWAP_PLUGINS` |               | Provide a comma-delimited list of HotSwap plugins to disable.  You can find a list of those plugins here: https://github.com/HotswapProjects/HotswapAgent/tree/master/plugin.  The name is only specified in the source, like [*Spring*](https://github.com/HotswapProjects/HotswapAgent/blob/master/plugin/hotswap-agent-spring-plugin/src/main/java/org/hotswap/agent/plugin/spring/SpringPlugin.java). |
| `ENABLE_JDWP`             | `true`        | Set to `false` to disable the Java debugger. |
| `JAVA_OPTS`               |               | Set to extend the standard `JAVA_OPTS` environment variable. |
