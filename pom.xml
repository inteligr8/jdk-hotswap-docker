<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.inteligr8</groupId>
    <artifactId>jdk-hotswap</artifactId>
    <version>2.0.1-jbr-17.0.12</version>
    <name>Java Hotswap Kit for Rapid Application Development</name>
    <packaging>pom</packaging>

    <properties>
        <!-- The release version of Ubuntu to use as the base -->
        <!-- See: https://hub.docker.com/_/ubuntu -->
        <!-- This has very little impact and may be replaced -->
        <ubuntu.version>24.10</ubuntu.version>

        <namespace.prefix>${project.artifactId}</namespace.prefix>

        <!-- The version of the Hotswap Agent to use -->
        <!-- See: https://github.com/HotswapProjects/HotswapAgent/releases -->
        <hotswap.version>2.0.1</hotswap.version>

        <!-- The version of the JetBrains JDK -->
        <!-- See: https://github.com/JetBrains/JetBrainsRuntime/releases -->
        <jbr.version>17.0.12</jbr.version>
        <jbr.buildNumber>1000.54</jbr.buildNumber>
        <jbr.basename>jbr_jcef-${jbr.version}-linux-x64-b${jbr.buildNumber}</jbr.basename>
        <jbr.filename>${jbr.basename}.tar.gz</jbr.filename>

        <!-- The Docker image meta-data for pushing the build -->
        <image.name>inteligr8/${project.artifactId}</image.name>
        <image.tag>${project.version}</image.tag>
        <image.registry>docker.inteligr8.com</image.registry>
    </properties>

    <build>
        <resources>
            <resource>
                <directory>src/main/docker</directory>
                <filtering>true</filtering>
                <targetPath>${project.build.directory}/resources</targetPath>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.3.1</version>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <phase>process-resources</phase>
                        <goals><goal>resources</goal></goals>
                        <configuration>
                            <encoding>utf-8</encoding>
                            <propertiesEncoding>utf-8</propertiesEncoding>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- This plugin build and pushes the Docker image -->
            <plugin>
                <groupId>org.eclipse.jkube</groupId>
                <artifactId>kubernetes-maven-plugin</artifactId>
                <version>1.16.2</version>
                <configuration>
                    <images>
                        <image>
                            <name>${image.name}:${image.tag}</name>
                            <registry>${image.registry}</registry>
                        </image>
                    </images>
                    <contextDir>${project.build.directory}/resources</contextDir>
                    <dockerFile>${project.basedir}/Dockerfile</dockerFile>
                    <verbose>true</verbose>
                </configuration>
                <executions>
                    <!-- This execution builds the Docker image -->
                    <execution>
                        <id>docker-build</id>
                        <phase>package</phase>
                        <goals><goal>build</goal></goals>
                    </execution>
                    <!-- This execution pushes the built Docker image to the configured Docker Registry -->
                    <execution>
                        <id>docker-push</id>
                        <phase>deploy</phase>
                        <goals><goal>push</goal></goals>
                    </execution>
                </executions>
            </plugin>
            <!-- This plugin prevents the project from deploying to the Maven Repository, as it is pointless -->
            <plugin>
            	<artifactId>maven-deploy-plugin</artifactId>
                <version>3.1.3</version>
            	<configuration>
            		<skip>true</skip>
            	</configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>no-docker</id>
            <properties>
                <jkube.build.strategy>jib</jkube.build.strategy>
            </properties>
        </profile>
    </profiles>
    
</project>
