
FROM ubuntu:${ubuntu.version}

# Configure standard JAVA parameters
ENV JAVA_MEMORY_INIT=128m
ENV JAVA_MEMORY_MAX=512m
ENV JAVA_HOME=/usr/local/lib/jvm/java-openjdk-jbr
ENV JRE_HOME=/usr/local/lib/jvm/java-openjdk-jbr
ENV JAVA_OPTS=
ENV ENABLE_HOTSWAP=true
ENV ENABLE_JDWP=true
ENV DISABLE_HOTSWAP_PLUGINS=

# Install curl & essentials
RUN apt update && \
        apt -y install curl libfreetype6

# Download & Install JBR Java
RUN mkdir -p /usr/local/lib/jvm && \
        curl -L https://cache-redirector.jetbrains.com/intellij-jbr/${jbr.filename} -o /usr/local/lib/jvm/java-openjdk-jbr.tar.gz && \
        cd /usr/local/lib/jvm && \
        tar xzvf java-openjdk-jbr.tar.gz && \
        mv ${jbr.basename} java-openjdk-jbr && \
        mkdir -p java-openjdk-jbr/lib/hotswap && \
        rm java-openjdk-jbr.tar.gz

# Download Hotswap Agent
RUN curl -L https://github.com/HotswapProjects/HotswapAgent/releases/download/RELEASE-${hotswap.version}/hotswap-agent-${hotswap.version}.jar -o /usr/local/lib/jvm/java-openjdk-jbr/lib/hotswap/hotswap-agent.jar

# Add directories for dynamic injection points
RUN mkdir -p /var/lib/jvm && cd /var/lib/jvm && \
        mkdir dev lib && cd dev && \
        mkdir classes classes-extra1 classes-extra2 classes-extra3 classes-extra4 classes-extra5 classes-extra6 classes-extra7 && \
        mkdir lib lib-extra1 lib-extra2 lib-extra3 lib-extra4 lib-extra5 lib-extra6 lib-extra7

# Add our Docker container initialization scripts
ADD maven/target/resources/setenv.sh /usr/local/bin/${namespace.prefix}-setenv.sh
ADD maven/target/resources/docker-entrypoint.sh /usr/local/bin
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

# Add our default hotswap configuration; may be overwritten in dev/classes* volumes
ADD maven/target/resources/hotswap-agent.properties /var/lib/jvm/lib

# Listening for Java debugger traffic
EXPOSE 8000

# Running as ROOT user for now
#USER java

# Execute the Docker container initialization script
ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]
