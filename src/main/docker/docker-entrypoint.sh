#!/bin/sh
. /usr/local/bin/${namespace.prefix}-setenv.sh

JAVA_CP="/var/lib/jvm/lib:/var/lib/jvm/lib/*"
JAVA_CP="${JAVA_CP}:/var/lib/jvm/dev/classes:/var/lib/jvm/dev/classes-extra1:/var/lib/jvm/dev/classes-extra2:/var/lib/jvm/dev/classes-extra3:/var/lib/jvm/dev/classes-extra4:/var/lib/jvm/dev/classes-extra5:/var/lib/jvm/dev/classes-extra6:/var/lib/jvm/dev/classes-extra7"
JAVA_CP="${JAVA_CP}:/var/lib/jvm/dev/lib/*:/var/lib/jvm/dev/lib-extra1/*:/var/lib/jvm/dev/lib-extra2/*:/var/lib/jvm/dev/lib-extra3/*:/var/lib/jvm/dev/lib-extra4/*:/var/lib/jvm/dev/lib-extra5/*:/var/lib/jvm/dev/lib-extra6/*:/var/lib/jvm/dev/lib-extra7/*"
EXEC_JAVA_OPTS="${JAVA_OPTS} -classpath ${JAVA_CP}"

exec ${JAVA_HOME}/bin/java ${EXEC_JAVA_OPTS} ${JAVA_CLASS}
