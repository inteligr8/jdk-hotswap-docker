#!/bin/sh
JAVA_OPTS="${JAVA_OPTS} -Xms${JAVA_MEMORY_INIT} -Xmx${JAVA_MEMORY_MAX}"

if [ "${ENABLE_HOTSWAP}" = "true" ]; then
    JAVA_OPTS="${JAVA_OPTS} -XX:+AllowEnhancedClassRedefinition -XX:HotswapAgent=fatjar"
    JAVA_OPTS="${JAVA_OPTS} -Dhotswap.disablePlugins=${DISABLE_HOTSWAP_PLUGINS}"
fi

if [ "${ENABLE_JDWP}" = "true" ]; then
    JAVA_OPTS="${JAVA_OPTS} -agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n"
fi
